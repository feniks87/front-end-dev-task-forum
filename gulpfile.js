const { dest, src, watch: gWatch, task } = require('gulp');
const sass = require('gulp-sass');
const postcss = require('gulp-postcss');
const autoprefixer = require('autoprefixer');
const cssnano = require('cssnano');
const sourcemaps = require('gulp-sourcemaps');

const paths = {
  styles: {
    src: 'scss/**/*.scss',
    dest: 'css'
  }
};

function style() {
  const stylelint = require('gulp-stylelint');

  return (
    src('scss/*scss')
      .pipe(stylelint({
        failAfterError: false,
        reporters: [
          {formatter: 'string', console: true}
        ]
      }))
      .pipe(sourcemaps.init())
      .pipe(sass())
      .on("error", sass.logError)
      .pipe(postcss([autoprefixer(), cssnano()]))
      .pipe(sourcemaps.write())
      .pipe(dest('css'))
  );
}

function watch() {
  style();

  gWatch(paths.styles.src, style);
}

task('default', () => {
  return style();
});

exports.default = style;
exports.style = style;
exports.watch = watch;
