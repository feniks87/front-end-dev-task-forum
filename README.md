# FRONT END DEV TASK - FORUM #

This a minimum setup of scrapped site for amazing Front End Developer to show off their design skills.

### Welcome to Communica's Forum Site Design Task ###

### Setup
1. Create yourself a bitbucket account
2. Fork this repository by clicking the **+** button on the left panel, choose **Fork this repository**
3. Clone the forked repository to your local machine
    1. Click the **Clone** from the top right screen
    2. Copy the git command, paste and run it on your terminal 
4. Run `yarn install` from project root to install necessary packages

### Compiling SCSS into CSS
In Communica, we use gulpjs to compile all of our scss files.

To execute gulp command, run the following from project root
`./node_modules/.bin/gulp`

To watch scss files during your development, add `watch` after gulp command

### Tasks
This is a scrapped forum page of wrike.com, your missions are -

- Tweak it and re-brand it to look like the [orchid.systems](https://www.orchid.systems/) site
- Improve its layout and design

#### REQUIREMENTS
- Site must be styled in using the brand colors of [orchid.systems](https://www.orchid.systems/)
- Stylesheet needs to be written in the format of **scss**

#### COMPLETION PROCESS
- Commit and push your changes to your repository
- Create a pull request to its original repository

###### Pro tip
_* Feel free to ask for help if you are stuck :)_
